function R=rand(pD,nData)
%R=rand(pD,nData) returns random scalars drawn from given Discrete Distribution.
%
%Input:
%pD=    DiscreteD object
%nData= scalar defining number of wanted random data elements
%
%Result:
%R= row vector with integer random data drawn from the DiscreteD object pD
%   (size(R)= [1, nData]
%
%----------------------------------------------------
%Code Authors:  Samiul Alam (samiula@kth.se), Mark Wong (markwong@kth.se)
%Course Code:   EQ2340
%Assignment no: 1
%----------------------------------------------------

if numel(pD)>1
    error('Method works only for a single DiscreteD object');
end;

%*** Insert your own code here and remove the following error message 
intervals=cumsum(pD.ProbMass);
R=zeros(1, nData); %Preallocate R
for i=1:nData
    R(i)=find(rand(1)<intervals,1); %Draw observation from Discrete distribution
end