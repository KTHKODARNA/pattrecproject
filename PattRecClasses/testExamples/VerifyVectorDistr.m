%A simple infinite-duration HMM where the outputs are Gaussian vector distributions.

%Code Authors:  Samiul Alam (samiula@kth.se), Mark Wong (markwong@kth.se)
%Course Code:   EQ2340
%Assignment no: 1

mc=MarkovChain([1 0], [0.9 0.1; 0.3 0.7]); %State generator
cov=[4 2; 2 25]; %Covariance matrix for g1
g1=GaussD('Mean',[1 3],'StDev',[2 5],'Covariance',cov); %Distribution for state=1
g2=GaussD('Mean',[7 9],'StDev',[2 5]); %Distribution for state=2
h=HMM(mc, [g1;g2]); %The HMM
x=rand(h, 150) %Generate an output sequence