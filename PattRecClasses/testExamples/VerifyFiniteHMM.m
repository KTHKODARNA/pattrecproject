%Code Authors:  Samiul Alam (samiula@kth.se), Mark Wong (markwong@kth.se)
%Course Code:   EQ2340
%Assignment no: 1

q=[1]; %Initial probability vector
A=[0.9 0.1]; %Transition probability matrix

mc=MarkovChain(q,A); %State generator

g1=GaussD('Mean',0,'StDev',1); %Distribution for state=1

h=HMM(mc, g1); %Finite-state HMM

%A simple test to see how often a sequence consists of:
%CASE 1:   State=1 followed by State=1 (length=2)
%CASE 2:   State=1 followed by END state (length=1)
%Output:   Frequency of case 2: should be about 10% of the iterations
%(probability of entering state 2).
lengths=zeros(1,500);
for i=1:500
    x=rand(h,2);
    lengths(i)=length(x);
end
sum(lengths==1)/sum(lengths==2)