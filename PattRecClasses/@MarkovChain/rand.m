function S=rand(mc,T)
%S=rand(mc,T) returns a random state sequence from given MarkovChain object.
%
%Input:
%mc=    a single MarkovChain object
%T= scalar defining maximum length of desired state sequence.
%   An infinite-duration MarkovChain always generates sequence of length=T
%   A finite-duration MarkovChain may return shorter sequence,
%   if END state was reached before T samples.
%
%Result:
%S= integer row vector with random state sequence,
%   NOT INCLUDING the END state,
%   even if encountered within T samples
%If mc has INFINITE duration,
%   length(S) == T
%If mc has FINITE duration,
%   length(S) <= T
%
%---------------------------------------------
%Code Authors:  Samiul Alam (samiula@kth.se), Mark Wong (markwong@kth.se)
%Course Code:   EQ2340
%Assignment no: 1
%---------------------------------------------

S=zeros(1,T);%Preallocate S for state sequence
nS=mc.nStates;%Number of states

%Infinite-duration HMM
if finiteDuration(mc)==0
    initialProbIntervals=cumsum(mc.InitialProb);
    startState=find(rand(1)<initialProbIntervals,1);
    S(1)=startState;
    currentState=startState;
    for i=2:T
        transitionProbs=mc.TransitionProb(currentState,:);
        hGen=DiscreteD(transitionProbs);
        nextState=rand(hGen,1);
        S(i)=nextState;
        currentState=nextState;
    end
else
    
    %Finite-duration HMM
    %If S(t)=nStates+1 then we have reached the END state.
    %return S after removing trailing zeros.
    initialProbIntervals=cumsum(mc.InitialProb);
    startState=find(rand(1)<initialProbIntervals,1);
    S(1) = startState;
    currentState = startState;
    if currentState == nS+1
        S = [];
        return
    end
    for i=2:T
        transitionProbs = mc.TransitionProb(currentState,:);
        hGen=DiscreteD(transitionProbs);
        nextState=rand(hGen, 1);
        if nextState == nS+1
            break
        else
            S(i) = nextState;
        end
        currentState = nextState;
    end
    %Remove trailing zeros
    zeroIndex = find(S, 1, 'last');
    S=S(1:zeroIndex);
end
