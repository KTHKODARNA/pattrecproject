function Character=Classifier(raw_data)
    load('trainedModels')           % Load HMMs
    features = Main(raw_data);      % Extract features
    x=reshape(features',[1,16*16]); % Reshape into vector
    x=x+1;                          % S=1 or S=2
    logprobs=logprob(hmm,x);       % Compute log probabilities
    probabilities = exp(logprobs);  % Compute probabilites
    [M, Character] = max(probabilities);    % Find index of largest probability

    if Character==10
        0
    else
        Character
    end
%    exp(logprobs)
end
% 1. Introduce yourself (Namn+email, masterprogram, inriktning)
% 2. Multiplication for kids (numbers 0-9)
% 3. Describe your feature extractor scheme:
%        - Discrete features
%        - Scalars (0 or 1)
%        - Variations in size, shape and position
%          (Normalize/Scale/Grid(hist3)/etc.)
%        - Feature extractor scheme:
%          a) Remove zeros (information between pen strokes)
%          b) Normalize Character (scale and center)
%          c) Create Grid (bivariate histogram)
%          d) Normalize histogram (1 or 0)+1 to get feature vector.
% 4. HMM design:
%        - 256 states: Grid with size 16x16
%        - Left-Right HMM, always next state after 1 "t"
%        - DiscreteD (either output=1 pr output=2)
% 5. Training and Testing:
%        - Dataset with 20 labeled samples per symbol
%        - Randomly shuffled, then 15-5 (training-testing),
%          then test error rate averaged over many iterations
% 6. Plots and description/discussion
% 7. Average classificatin error over test set, most commonly
%    misclassified...
% 8. Confusion matrix + n?gon bild
% 9. Live demo and conclusions
% 0. Weakness: tilted symbols very confusing (bad for kids)