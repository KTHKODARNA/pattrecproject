function [n, dat] = CreateGrid( Xs, Ys )
%CREATEGRID Summary of this function goes here
%   Detailed explanation goes here

%http://se.mathworks.com/help/stats/hist3.html
dat=[Xs' Ys'; 0 0; 0 1; 1 0; 1 1]; %Kalibreringspunkter
gridSize=[16 16];
n=hist3(dat,gridSize);
n1=n';
n1(n1>0)=1; %Endast unika punkter
n=n1;
end

