function featureVector=Main(raw_data)
% Feature extractor
no_zeros_data=RemoveZeros(raw_data); %Remove points where the pen made no mark
[Xs, Ys]=NormalizeCharacter(no_zeros_data); %Scale and center character to grid
[n, dat]=CreateGrid(Xs, Ys); %Create grid
%PlotHistogram(n, dat); %Plot character on grid
%n is our feature matrix. You can roll it out to a long vector if you want.
featureVector=n;
end