load('oneModel')
hmm(1)=hmmTrained;

load('twoModel')
hmm(2)=hmmTrained;

load('threeModel')
hmm(3)=hmmTrained;

load('fourModel')
hmm(4)=hmmTrained;

load('fiveModel')
hmm(5)=hmmTrained;

load('sixModel')
hmm(6)=hmmTrained;

load('sevenModel')
hmm(7)=hmmTrained;

load('eightModel')
hmm(8)=hmmTrained;

load('nineModel')
hmm(9)=hmmTrained;

load('zeroModel')
hmm(10)=hmmTrained;

for j=1:10
    for i=1:16
         a=hmm(1,j).OutputDistr(i).ProbMass+0.001;
         a=a/sum(a);
         hmm(1,j).OutputDistr(i).ProbMass=a;
    end
end

save('trainedModels','hmm')