function [Xs, Ys] = NormalizeCharacter( xybout )
%NORMALIZECHARACTER Summary of this function goes here
%   Detailed explanation goes here

%Split data into Xs and Ys
Xs=xybout(1,:); Ys=xybout(2,:);

%Put character into grid (scale etc.)
characterWidth=max(Xs)-min(Xs);
characterHeight=max(Ys)-min(Ys);

if characterWidth<characterHeight
    %Put character on bottom of grid
    Ys=Ys-min(Ys);
    
    %Normalize/scale
    factor = max(Ys);
    Ys=Ys/factor;
    Xs=Xs/factor;
    
    %Center
    Xs=Xs+(0.5-mean(Xs));
    
else %if height is greater than width
    %Put character on side of grid
    Xs=Xs-min(Xs);
    
    %Normalize/scale
    factor = max(Xs);
    Xs=Xs/factor;
    Ys=Ys/factor;
    
    %Center
    Ys=Ys+(0.5-mean(Ys));
end
end

