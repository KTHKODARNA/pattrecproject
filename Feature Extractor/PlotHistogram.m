function PlotHistogram(n, dat)
%PLOTHISTOGRAM Summary of this function goes here
%   Detailed explanation goes here
n1=n';
% Code for generating plots
n(size(n1,1) + 1, size(n1,2) + 1) = 0;
xb = linspace(min(dat(:,1)),max(dat(:,1)),size(n1,1)+1);
yb = linspace(min(dat(:,2)),max(dat(:,2)),size(n1,1)+1);
h=pcolor(xb,yb,n);

end

