disp('Classifying...')
load('trainedModels')
load('nineTest')
for i=1:5
    features=testset(:,i);
    x=reshape(features',[1,16*16]); % Reshape into vector
    x=x+1;                          % S=1 or S=2
    logprobs=logprob(hmm,x);       % Compute log probabilities
    probabilities = exp(logprobs);  % Compute probabilites
    [M, Character] = max(probabilities);    % Find index of largest probability

    if Character==10
        0
    else
        Character
    end
end